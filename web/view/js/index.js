function inicio() {
	departamento()
}

function departamento() {
	$.ajax({
		type: "POST",
		url: "../service/departamento.php",
		data: {},
		success: function(response) {
			//console.log(response);
			$("#combodepartamento").html(response)
		}
	})
}

function provincia(id_departamento) {
	$.ajax({
		type: "POST",
		url: "../service/provincia.php",
		data: { id_departamento: id_departamento },
		success: function(response) {
			//console.log(response);
			$("#comboprovincia").html(response)
		}
	})
}

function distrito(id_departamento, id_provincia) {
	$.ajax({
		type: "POST",
		url: "../service/distrito.php",
		data: { id_departamento: id_departamento, id_provincia: id_provincia },
		success: function(response) {
			//console.log(response);
			$("#combodistrito").html(response)
		}
	})
}

$(document).on("change", "#id_departamento", function() {
	let id_departamento = $("#id_departamento").val()

	$("#comboprovincia").html("")
	$("#combodistrito").html("")

	provincia(id_departamento)
})

$(document).on("change", "#id_provincia", function() {
	let id_departamento = $("#id_departamento").val()
	let id_provincia = $("#id_provincia").val()

	$("#combodistrito").html("")

	distrito(id_departamento, id_provincia)
})

inicio()
