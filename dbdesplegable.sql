-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: May 11, 2019 at 10:27 PM
-- Server version: 8.0.13
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbdesplegable`
--

-- --------------------------------------------------------

--
-- Table structure for table `departamento`
--

CREATE TABLE `departamento` (
  `id_departamento` char(2) NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `departamento`
--

INSERT INTO `departamento` (`id_departamento`, `nombre`) VALUES
('01', 'Lambayeque'),
('02', 'La Libertad');

-- --------------------------------------------------------

--
-- Table structure for table `distrito`
--

CREATE TABLE `distrito` (
  `id_departamento` char(2) NOT NULL,
  `id_provincia` char(2) NOT NULL,
  `id_distrito` char(2) NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `distrito`
--

INSERT INTO `distrito` (`id_departamento`, `id_provincia`, `id_distrito`, `nombre`) VALUES
('01', '01', '01', 'Chiclayo'),
('01', '01', '02', 'Chongoyape'),
('01', '01', '03', 'Eten'),
('01', '02', '01', 'Ferreñafe'),
('01', '02', '02', 'Cañaris'),
('01', '02', '03', 'Incahuasi'),
('01', '03', '01', 'Lambayeque'),
('01', '03', '02', 'Illimo'),
('01', '03', '03', 'Jayanca'),
('02', '01', '01', 'Trujillo'),
('02', '01', '02', 'El Porvenir'),
('02', '01', '03', 'Florencia De Mora'),
('02', '02', '01', 'Ascope'),
('02', '02', '02', 'Chicama'),
('02', '02', '03', 'Chocope');

-- --------------------------------------------------------

--
-- Table structure for table `provincia`
--

CREATE TABLE `provincia` (
  `id_departamento` char(2) NOT NULL,
  `id_provincia` char(2) NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `provincia`
--

INSERT INTO `provincia` (`id_departamento`, `id_provincia`, `nombre`) VALUES
('01', '01', 'Chiclayo'),
('01', '02', 'Ferreñafe'),
('01', '03', 'Lambayeque'),
('02', '01', 'Trujillo'),
('02', '02', 'Ascope');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id_departamento`);

--
-- Indexes for table `distrito`
--
ALTER TABLE `distrito`
  ADD PRIMARY KEY (`id_departamento`,`id_provincia`,`id_distrito`);

--
-- Indexes for table `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id_departamento`,`id_provincia`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `distrito`
--
ALTER TABLE `distrito`
  ADD CONSTRAINT `fk_distrito_provincia` FOREIGN KEY (`id_departamento`,`id_provincia`) REFERENCES `provincia` (`id_departamento`, `id_provincia`);

--
-- Constraints for table `provincia`
--
ALTER TABLE `provincia`
  ADD CONSTRAINT `fk_provincia_departamento` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id_departamento`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
